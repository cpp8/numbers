/*
 * number_test.cpp
 *
 *  Created on: Jan 19, 2021
 *      Author: RajaS
 */

#include <iostream>

using namespace std ;
#include "number.h"

int main1(int argc, char **argv)
{
	number magic(1056);
	number magicfromstr ;

	for (int b=2; b<32; b++) {
		std::string image = magic.Image(b) ;
		cout << "Base " << b << " : " << image << endl ;
		magicfromstr.Set( image ) ;
		if (magicfromstr.Value() != magic.Value()) {
			cout << "    Conversion back to binary failed" << endl ;
		} else {
			cout << "    Conversion to binary ok" << endl ;
		}
	}
	return 0 ;
}

void SqCubeExperiments(unsigned char base) {
	digits exp( base ) ;

	for (int i=0; i<1000; i++)
	{
		cout << i << " ," ;
		cout << i*i << " , " ;
		cout << i*i*i << " : " ;
		exp.Set(i) ;
		IntegerType sumsq = number::Sum( number::Square( exp.Value() )) ;
		IntegerType sumcube = number::Sum( number::Cube( exp.Value() )) ;

		cout << sumsq << " " << " ";
		cout << sumcube << endl ;
	}
}
int main2(int argc, char** argv)
{
	/*digits magic(10, 1999);
	digits magic3(10,0) ;
	sequence cube = number::Cube( magic.Value() );
	magic3.Set( cube );
	sequence sq = magic3.Value() ;
	for( sequence::iterator v=sq.begin(); v!=sq.end(); v++) {
		cout << *v << " " << endl ;
	}

	magic3.Set(sq) ;*/
	SqCubeExperiments(10) ;
}

int main3(int argc, char **argv)
{
	cout << "Palindromic test" << endl ;
	cout << "decimals" << endl ;
	unsigned char base=10;
	IntegerType num ;
	num = 1011 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;
	num = 10111101 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;
	num = 101171101 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;
	num = 1011771101 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;
	num = 1011781101 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;

	base = 8 ;
	cout << "octals" << endl ;
	num = 01011 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;
	num = 010111101 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;
	num = 0101171101 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;
	num = 01011771101 ; cout << num << " IsPalindrome=" << number::IsPalindromic( num , base ) << endl ;

}

int main4(int argc, char **argv)
{

	unsigned char base = 10 ;
	cout << "No     Harshad Happy   Base = " << (int)base << endl ;
	for (int h=10000; h<=11000 ; h++)
	{
		cout << h << "\t" ;
		cout << number::IsHarshad(h,base) << "\t" ;
		cout << number::IsHappy(h,base) ;
		cout << endl ;
	}
	base = 3 ;
	cout << "No     Harshad Happy   Base = " << (int)base << endl ;
	for (int h=10000; h<=11000 ; h++)
	{
		cout << h << "\t" ;
		cout << number::IsHarshad(h,base) << "\t" ;
		cout << number::IsHappy(h,base) ;
		cout << endl ;
	}

}

int main5(int argc, char **argv)
{
	IntegerType gcd = number::Gcd(1043008345 , 527452805 ) ;
	cout << gcd << endl ;
	IntegerType lcm = number::Lcm( 20 , 25) ;
	cout << lcm << endl ;
}

int main6(int argc, char **argv)
{
	factors f(65) ;
	sequence fd = f.Divisors() ;
	number::Show("Divisors" , fd) ;
	sequence ff = f.Factors() ;
	number::Show("Factors" , ff) ;
}

int main7(int argc, char **argv)
{
	unsigned candidates [] = {0, 3, 5, 6, 9, 10, 12, 15, 17, 18, 20, 23, 24, 27, 29, 30, 33, 34, 35, 34*2, 34*4 , 34*8 , 350, 3400 } ;

	for (int i=0; i<sizeof(candidates)/sizeof(unsigned) ; i++)
	{
		cout << candidates[i] << " evil? : " << number::IsEvil(candidates[i]) << endl ;
	}
}

int main8(int argc, char **argv)
{
	digits d(10,7992739871);
	cout << number(d).Value() << " " << (int)d.LuhnChecksum() << endl ;
	digits nd1 = d.AddLuhnChecksum() ;
	cout << nd1.Image() << endl ;
	cout << nd1.LuhnValid() << endl ;
	digits nd2(10,79927398715) ;
	cout << nd2.LuhnValid() << endl ;

	cout << "IsPrime " << number::IsPrime(16769023) << " => Is a Carol Prime" << endl ;
	cout << "IsPrime " << number::IsPrime(	193939) << " => Is a circular Prime" << endl ;

}

int main9(int argc, char **argv)
{
	IntegerType nos[] = { 6, 28, 496 , 8128 , 33550336 , 3355 , 336 , 1056 } ;

	for (int i=0; nos[i] != 1056 ;i++)
	{
		cout << nos[i] << " Perfect? " << number::IsPerfect(nos[i]) << endl ;
	}
	return EXIT_SUCCESS;
}

int main10(int argc, char **argv)
{
    taxicab::Enumerate(437,3);
    taxicab::Report();
}

int main(int argc, char **argv)
{
    IntegerType kaps[] = {55, 99, 297, 300, 444 , 703, 999999} ;
    for (int i=0; kaps[i] != 999999; i++)
    {
    	cout << kaps[i] << " Is Kaprekar? " << number::IsKaprekar(kaps[i]) << endl ;
    }
}
