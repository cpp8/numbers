/// @file  algorithms.cpp
/// @brief implementation of algorithms on numbers
/// @author R Srinivasn rs@toprllc.com

#include <iostream>
#include <algorithm>
#include "number.h"

// ********************************************************************************************
// Basic algorithms on numbers
// ********************************************************************************************


/// @brief Determine if the given number is Evil
/// @param _value - the number
/// @see https://en.wikipedia.org/wiki/Evil_number

bool number::IsEvil(IntegerType _value)
{
    int numbits = 0 ;
    IntegerType mask = 1 ;

    for (unsigned b=0; b<IntegerBits; b++)
    {
        if (_value & mask ) numbits ++ ;
        mask = mask << 1 ;
    }
    if (numbits & 1) return false ;
    return true ;
}

// ********************************************************************************************
// Basic algorithms on tuples of numbers
// ********************************************************************************************
IntegerType number::Gcd(IntegerType _me, IntegerType _other)
{
	std::cout << "Gcd " << _me << " " << _other << std::endl ;
	if ((_me <= 0) || (_other <= 0)) return 0 ;

	if (_me == _other) return _me ;

	if (_me < _other)
	{
		return Gcd(_other, _me) ;
	}
	IntegerType rem = _me % _other ;
	if (rem == 0) return _other ;

	return Gcd( _other , rem ) ;

}

IntegerType number::Lcm(IntegerType _me, IntegerType _other)
{
	IntegerType gcd = Gcd(_me,_other) ;
	return (_me * _other) / gcd ;
}


IntegerType number::Gcd(IntegerType _other) const
{
	return Gcd( value , _other ) ;
}

IntegerType number::Lcm(IntegerType _other) const
{
	return Lcm( value , _other ) ;
}

bool number::MutualPrime(IntegerType _other) const
{
	IntegerType gcd = Gcd(_other) ;
	if (gcd == 1) return true ;
	return false ;
}

// ********************************************************************************************
// Algorithms based on digitization in specific number systems
// ********************************************************************************************

/// @brief Determines if the given number expressed in the specified base is palindromic
///
/// @param _value - the value being tested
/// @param _base  - the base. 2 for binary, 10 for decimal, 12 for duodecimal
///
/// @see https://en.wikipedia.org/wiki/Palindromic_number
 bool number::IsPalindromic(IntegerType _value, BaseType _base)
{
	digits digitize( _base , _value ) ;
	sequence val = digitize.Value() ;
	int numdigits = val.size() ;
	sequence::iterator left = val.begin() ;
	sequence::reverse_iterator right = val.rbegin() ;
	for (int i=0; i<numdigits/2; i++)
	{
		if ((*left) != (*right))
		{
			return false ;
		}
		left++ ;
		right++ ;
	}
	return true ;
}

/// @brief Determines if the given number expressed in the specified base is a Harshad Number
/// @param _value - the value being tested
/// @param _base  - the base. 2 for binary, 10 for decimal, 12 for duodecimal
///
/// @see https://mathworld.wolfram.com/HarshadNumber.html
/// @see https://en.wikipedia.org/wiki/Harshad_number
 bool number::IsHarshad(IntegerType _value, BaseType _base)
{
	digits harshad( _base , _value ) ;
	sequence hdigits = harshad.Value() ;
	unsigned long sumdigits = number::Sum(hdigits) ;
	if (( _value % sumdigits) == 0)
	{
		return true ;
	}
	return false ;
}
/// @brief Determines if the given number expressed in the specified bases is a Happy number
///
/// @param _value - the value being tested
/// @param _base  - the base. 2 for binary, 10 for decimal, 12 for duodecimal
///
/// References:
/// @see https://mathworld.wolfram.com/HappyNumber.html
/// @see https://en.wikipedia.org/wiki/Happy_number
bool number::IsHappy(IntegerType _value, BaseType _base)
{
    sequence seen ;

    int loopcount=0 ;
    digits happy( _base , _value) ;
    while (loopcount < 1000)
    {
    	sequence hdigits = happy.Value() ;
    	sequence hdigitssq = number::Square(hdigits) ;
    	IntegerType hnumber = number::Sum(hdigitssq) ;
    	if (hnumber == 1)
    	{
    		return true ;
    	}

    	if (hnumber < 10)
    	{
    		return false ;
    	}
    	loopcount ++ ;

    	sequence::iterator found = std::find( seen.begin(), seen.end() , hnumber ) ;
    	if (found != seen.end())
    	{
    		return false ;
    	}

    	seen.push_back(hnumber) ;
    	happy.Set(hnumber) ;
    }
    return false ;
}
/// @brief Determines if the given number is a Kaprekar number
///
/// @param _value - the value being tested
///
/// References:
/// @see https://en.wikipedia.org/wiki/Kaprekar_number
/// @see https://mathworld.wolfram.com/KaprekarNumber.html
bool number::IsKaprekar(IntegerType _value)
{
	IntegerType kapsq = _value * _value ;
	digits kapdigs(10,kapsq) ;
	sequence digs = kapdigs.Value();
	//std::cout << "Square " << kapsq << std::endl ;
	for (unsigned left=1; left < digs.size()-1; left++)
	{
		IntegerType lsum=0;
		IntegerType rsum=0;
		unsigned numdigs=1;
		for (sequence::iterator dptr=digs.begin(); dptr != digs.end(); dptr++)
		{
			if (numdigs++ <= left)
			{
			   lsum = lsum*10 + (*dptr) ;
			}
			else
			{
				rsum = rsum*10 + (*dptr) ;
			}
		}
		//std::cout << "Lsum " << lsum << " Rsum " << rsum << std::endl ;
		if (_value == (lsum + rsum))
		{
			return true ;
		}
	}
	return false ;
}
// ********************************************************************************************
// Algorithms based on factors
// ********************************************************************************************

/// @brief Determine if a given number is Prime
/// @param _value - the number to test
/// @see   https://en.wikipedia.org/wiki/Prime_number
/// @note  This is a simplistic implementation by counting the number of factors
bool number::IsPrime(IntegerType _value)
{
    factors f(_value) ;
    sequence ffactors = f.Factors() ;
    if (ffactors.size() > 2) {
    	return false ;
    }
    return true ;
}
/// @brief Determines if the given number is Perfect
///
/// @param _value - the value being tested
///
/// @see https://en.wikipedia.org/wiki/Perfect_number
bool number::IsPerfect(IntegerType _value)
{
	factors f(_value) ;
	sequence divs = f.Divisors() ;
	IntegerType divsum = Sum(divs) ;
	if (divsum/2 == _value)
	{
		return true ;
	}
	return false ;
}
// ********************************************************************************************
// Algorithms based on factors and digitization
// ********************************************************************************************

/// @brief Determines if the given number expressed in the specified bases is a Circular Prime
///
/// @param _value - the value being tested
/// @param _base  - the base. 2 for binary, 10 for decimal, 12 for duodecimal
///
/// @see https://en.wikipedia.org/wiki/Circular_prime
bool number::IsCircularPrime(IntegerType _value, BaseType _base)
{
    digits d(_base,_value) ;
    sequence dsec = d.Value() ;
    int numdigits = dsec.size() ;
    digits rd(_base,_value) ;
    for (int i=0; i<numdigits ; i++)
    {
        if (!number::IsPrime(rd.number::Value()))
        {
        	return false ;
        }
        sequence temp = rd.Rotate().Value() ;
        rd.Set( temp ) ;
    }
    return true ;
}

