/// @file  taxicab.cpp
/// @brief implementation of taxicab searches
/// @author R Srinivasn rs@toprllc.com

#include <iostream>
#include <algorithm>
#include <math.h>

#include "number.h"

taxicab::taxicab()
: number(0)
{

}

taxicab::~taxicab()
{
}

BaseType taxicab::power (0) ;
powersumtable taxicab::table ;

void taxicab::Enumerate(IntegerType _limit, BaseType _power)
{
	power = _power ;
	IntegerType sum =0;
	IntegerType largest = 0 ;
	for (IntegerType row=1; row <= _limit; row++)
	{
		for (IntegerType col=row ; col <= _limit; col++)
		{
			sum = pow(row,power) + pow(col,power) ;
			table.insert(std::make_pair(sum, std::make_pair(row,col))) ;
			if (sum > largest) largest = sum ;
		}
	}
	IntegerType key = 0 ;
	powersumtable::iterator tc=table.begin() ;
	while (tc != table.end())
	{
		key = tc->first ;
		if (table.count(key) >= 2)
		{
			tc++ ;
		}
		else
		{
			//std::cout << "Erasing key " << key << std::endl ;
			tc = table.erase(tc) ;
		}
		//std::cout << "Next key now is " << tc->first << std::endl ;
	}

}

void taxicab::Report()
{
	std::cout << "Power " << (int)power << " No " << table.size() << std::endl ;
	IntegerType key = 0 ;
	for (powersumtable::iterator tc=table.begin(); tc != table.end() ; tc++)
	{
		if (key != tc->first)
		{
			key = tc->first ;
			std::cout << key << std::endl ;
		}
		std::cout << "\t" << (tc->second).first << "," << (tc->second).second << std::endl ;
	}
	std::cout << "**********End Of Table*******************" << std::endl ;
}
