/*
 * number.cpp
 *
 *  Created on: Jan 19, 2021
 *      Author: RajaS
 */
#include <iostream>
#include "number.h"


std::string digitChars = "0123456789abcdefghijklmnopqrstuvwxyz" ;

number::number()
: value(0)
{
}

number::number(IntegerType _value)
{
	value = _value ;
}

number::~number() {

}

IntegerType number::Set(std::string _value)
{
	size_t pos=0;

	pos = _value.find(delimiter);
	std::string numstr = _value.substr(0, pos) ;
	std::string basestr = _value.substr(pos+1);

	unsigned int base = std::stoi(basestr) ;
	if (base >= digitChars.length()) {
		std::cout << "Base out of range " << basestr << std::endl ;
		return value ;
	}

	std::cout << "Base set to " << base << std::endl ;
	IntegerType newvalue = 0 ;
	std::string::iterator d ;
	size_t dpos ;
	for ( d = numstr.begin(); d != numstr.end() ; d++) {
		dpos = digitChars.find( *d ) ;
		newvalue *= base ;
		newvalue += dpos ;
	}
	// std::cout << "Setting new value " << newvalue << std::endl ;
	value = newvalue ;
	return value ;
}

std::string number::Image(BaseType base)
{
	if (base > digitChars.length()) {
		return "<<InvalidBase>>" ;
	}
	std::string basestr ;
	basestr = std::to_string(base) ;

	if (value == 0) {
		return "0" +delimiter +basestr ;
	}

	std::string result ;
	IntegerType temp = value ;
	BaseType nextdig ;
	BaseType nextdigstr ;

	while (temp > 0) {
		nextdig = temp % base ;
		nextdigstr = digitChars.at(nextdig) ;
		result.insert(0,1,nextdigstr) ;
		temp = temp / base ;
	}
	return result + delimiter + basestr ;
}


IntegerType number::Sum(sequence _value)
{
	IntegerType result = 0;
	for (sequence::iterator v = _value.begin() ; v != _value.end(); v++)
	{
		result += (*v) ;
	}
	return result ;
}

IntegerType number::Product(sequence _value)
{
	IntegerType result = 0;
	for (sequence::iterator v = _value.begin() ; v != _value.end(); v++)
	{
		result *= (*v) ;
	}
	return result ;
}

sequence number::Square(sequence _value)
{
	sequence result ;
	for (sequence::iterator v = _value.begin() ; v != _value.end(); v++)
	{
		result.push_back( (*v) * (*v) ) ;
	}
	return result ;
}

sequence number::Cube(sequence _value )
{
	sequence result ;
	for (sequence::iterator v = _value.begin() ; v != _value.end(); v++)
	{
		result.push_back( (*v) * (*v) * (*v) ) ;
	}
	return result ;
}


void number::Show(std::string _title , sequence _value)
{
	std::cout << _title << " : " ;
	for (sequence::iterator v=_value.begin(); v != _value.end(); v++)
	{
		std::cout << (*v) << "," ;
	}
	std::cout << std::endl ;
}
