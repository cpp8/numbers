/*
 * digits.cpp
 *
 *  Created on: Jan 21, 2021
 *      Author: RajaS
 */

#include <iostream>
#include "number.h"

digits::digits(BaseType _base)
: number(0)
{
	base = _base ;
}


digits::~digits()
{
    digitized.clear() ;
}

digits::digits(BaseType _base, IntegerType _value)
: number(0) , base(_base)
{
	sequence newseq = Set(_value) ;
}


std::string digits::Image()
{
	return number::Image(base) ;
}

sequence digits::Set(IntegerType _value)
{
	value = _value ;
	sequence result ;

	IntegerType temp = value ;
	BaseType nextdig ;

	while (temp > 0) {
		nextdig = temp % base ;
		result.insert(result.begin(),1,nextdig) ;
		temp = temp / base ;
	}
	digitized = result ;
	return digitized ;
}

sequence digits::Value()
{
	return digitized ;
}

IntegerType digits::Set(sequence& _value)
{
	IntegerType result = 0;
	int pos=0;

	for (sequence::reverse_iterator v = _value.rbegin() ; v != _value.rend(); v++, pos++ )
	{
		result += (*v) ^ pos ;
	}
	value = result ;
	digitized = _value ;
	return result ;
}

BaseType digits::LuhnChecksum()
{
	IntegerType checksum=0 ;
	bool dbldig=true ;
	for (sequence::reverse_iterator d = digitized.rbegin(); d != digitized.rend(); d++)
	{
		if (dbldig) {
			IntegerType newdigit = 2 * (*d) ;
			IntegerType d1 = newdigit % base ;
			IntegerType d2 = newdigit / base ;
			checksum += d1 + d2 ;
		}
		else
		{
			checksum += (*d) ;
		}
		dbldig = !dbldig ;
	}
	checksum *= (base - 1 );
	BaseType checkdig = checksum % base ;
	return checkdig ;
}

digits digits::AddLuhnChecksum()
{
	digits result (base) ;
    BaseType cs = LuhnChecksum() ;
    result.Set( number::Value() * base + cs ) ;
    return result ;
}

bool digits::LuhnValid()
{
	digits wocs (base , number::Value() / base ) ;
	BaseType cs = wocs.LuhnChecksum() ;
	if (cs == *digitized.rbegin())
	{
		return true ;
	}
	return false ;
}

/// @brief Rotate the digits of the number and return the new number
/// @param left - indicates the direction of rotation left vs right
/// @returns digits - a new digits object
digits digits::Rotate(bool left)
{
	if (left)
	{
	    sequence seq = this->Value() ;
	    sequence::iterator leftmostp=seq.begin() ;
	    IntegerType leftmost = (*leftmostp) ;
	    seq.erase(seq.begin()) ;
	    seq.push_back(leftmost) ;
	    digits result(base) ;
	    result.Set(seq) ;
	    return result ;
	}

    sequence seq = this->Value() ;
    sequence::reverse_iterator rightmostp=seq.rbegin() ;
    IntegerType rightmost = (*rightmostp) ;
    seq.pop_back() ;
    seq.insert(seq.begin(),1,rightmost) ;
    //seq.push_front(rightmost) ;
    digits result(base) ;
    result.Set(seq) ;
    return result ;
}

