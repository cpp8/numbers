/*
 * factors.cpp
 *
 *  Created on: Jan 27, 2021
 *      Author: RajaS
 */


#include <iostream>
#include <math.h>
#include <algorithm>

#include "number.h"


factors::factors(IntegerType _value)
: number(_value)
{
	Set(_value) ;
}

factors::~factors()
{
   allfactors.clear() ;
}

sequence factors::Set(IntegerType _value)
{
	value = _value ;
	sequence result ;
	IntegerType lim = (IntegerType)sqrt((double)value) ;
	IntegerType temp = value ;
	//result.push_back(1) ;
	for (IntegerType f = 2; (f < lim) && temp > 1 ; f++)
	{
		while ((temp >= f) && ((temp % f ) == 0))
		{
			result.push_back( f ) ;
			temp /= f ;
		}
	}
	if (temp != 1)
	{
		result.insert(result.begin(),1,1) ;
	}
	result.insert(result.begin(),1,temp) ;
	std::sort(result.begin(), result.end()) ;
	allfactors = result ;
	return result ;
}

sequence factors::Divisors()
{
	sequence divisors ;
	IntegerType lim = (IntegerType)sqrt((double)value) ;
	divisors.push_back(1) ;
	divisors.push_back(value) ;
	for (IntegerType d=2; d <= lim ; d++)
	{
		if ((value % d) == 0)
		{
		    divisors.push_back( d ) ;
		    divisors.push_back( value / d ) ;
		}
	}
	std::sort(divisors.begin() , divisors.end() )  ;
	return divisors ;
}
