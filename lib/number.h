
/// @file
/// @author  R Srinivasan <rs@toprllc.com>
/// @version 1.0
/// @section DESCRIPTION
///
/// This is the interface to a library to support amusements with numbers.
///

#ifndef LIB_NUMBER_H_
#define LIB_NUMBER_H_

#include <string>
#include <vector>
#include <map>

typedef unsigned long long IntegerType;                /// Customize for 32 or 64 bits
const unsigned IntegerBits = sizeof(IntegerType) * 8 ; /// Number of bits in the integer

typedef unsigned char BaseType ;                       /// Numbers in any base - binary, octal, decimal and
                                                       /// hexadecimal are common
const unsigned BaseBits = sizeof(BaseType) * 8 ;

typedef std::vector<IntegerType> sequence ;
typedef std::pair<IntegerType,IntegerType> numpair ;
typedef std::multimap<IntegerType,numpair> powersumtable ;

/// \class number
/// \brief base class for many numerical algorithms
///
/// In text form, numbers are specified as value # base
/// where the base - always specified as a decimal value indicates how to interpret
/// the value. so 1200#16 is in hexadecimal whereas 1200#8 is octal.

/// This library supports arbitrary bases in the range 2 to 36
/// if the base is 10 digits are in the range '0' to '9'
///                12                         '0' to 'b'
///                18                         '0' to 'h'
/// and so on.
class number {
public:
	const std::string delimiter = "#" ;

	number();
	number(IntegerType _value);
	virtual ~number();

	IntegerType Value() { return value ; }
	IntegerType Set(std::string _value) ;
	std::string Image(BaseType base) ;

	IntegerType Gcd(IntegerType _other) const ;
	IntegerType Lcm(IntegerType _other) const ;
	bool MutualPrime(IntegerType _other) const ;

	static IntegerType Gcd(IntegerType _me, IntegerType _other) ;
	static IntegerType Lcm(IntegerType _me, IntegerType _other) ;

	static void Show(std::string _title , sequence _value) ;

	static IntegerType Sum(sequence _value) ;
	static IntegerType Product(sequence _value) ;
	static sequence Square(sequence _value_) ;
	static sequence Cube(sequence _value ) ;

	static bool IsPalindromic(IntegerType _value, BaseType _base=10) ;
	static bool IsHarshad(IntegerType _value, BaseType _base=10) ;
	static bool IsHappy(IntegerType _value, BaseType _base=10) ;
	static bool IsPrime(IntegerType _value) ;
	static bool IsCircularPrime(IntegerType _value, BaseType _base=10) ;
	static bool IsPerfect(IntegerType _value) ;
	static bool IsKaprekar(IntegerType _value) ;
	static bool IsEvil(IntegerType _value) ;

protected:
	IntegerType value ;
};

class digits : public number {
public:
	digits(BaseType _base) ;
	virtual ~digits() ;
	digits(BaseType _base, IntegerType _value) ;
	sequence Set(IntegerType _value) ;
	IntegerType Set(sequence& _value) ;
	sequence Value() ;
	sequence Digits() { return digitized ; }

	digits Rotate(bool left=true) ;
	std::string Image() ;

	BaseType LuhnChecksum()  ;
	digits AddLuhnChecksum()  ;
	bool LuhnValid()  ;

private:
	BaseType base ;
	sequence digitized ;
};


class factors : public number {
public:
	factors(IntegerType _value) ;
	virtual ~factors() ;
	sequence Set(IntegerType _value) ;
	sequence Divisors() ;
	sequence Factors() { return allfactors ; }
private:
	sequence allfactors ;
};

class taxicab : public number {
public:
	taxicab() ;
	virtual ~taxicab() ;
	static void Enumerate(IntegerType _limit=100, BaseType _power=3) ;
	static void Report() ;
private:
	static BaseType power ;
	numpair parts ;
	static powersumtable table ;
};

#endif /* LIB_NUMBER_H_ */
